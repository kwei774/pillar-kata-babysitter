#include <string>



#ifndef SIMPLETIME_H
#define SIMPLETIME_H

struct simpletime{
    int hour;
    int minute;
    simpletime & operator =(const simpletime & other);
    int roundTime();
};

bool operator < (const simpletime &lhs, const simpletime &rhs);
bool operator <= (const simpletime &lhs, const simpletime &rhs);
bool operator > (const simpletime &lhs, const simpletime &rhs);
bool operator >= (const simpletime &lhs, const simpletime &rhs);
bool operator == (const simpletime &lhs, const simpletime &rhs);
bool operator != (const simpletime &lhs, const simpletime &rhs);

simpletime operator - (const simpletime &lhs, const simpletime &rhs);


#endif
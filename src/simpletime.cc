#include "simpletime.h"

bool operator < (const simpletime &lhs, const simpletime &rhs) {
    if (lhs.hour == rhs.hour){
        return lhs.minute < rhs.minute;
    }
    else{
        return lhs.hour < rhs.hour;
    }
}
bool operator <= (const simpletime &lhs, const simpletime &rhs) {
    return !(rhs < lhs);
}

bool operator > (const simpletime &lhs, const simpletime &rhs) {
    return rhs < lhs;
}

bool operator >= (const simpletime &lhs, const simpletime &rhs) {
    return !(lhs < rhs);
}

bool operator == (const simpletime &lhs, const simpletime &rhs) {
    return (lhs.hour == rhs.hour && lhs.minute == rhs.minute);
}

bool operator != (const simpletime &lhs, const simpletime &rhs) {
    return !(lhs == rhs);
}

simpletime & simpletime::operator =(const simpletime & other){
    this->hour = other.hour;
    this->minute = other.minute;
    return *this;
}

simpletime operator -(const simpletime &lhs, const simpletime &rhs){
    simpletime result;
    int carryoverMinute = (lhs.minute < rhs.minute ? 60 : 0);
    int carryoverHour = (lhs.minute < rhs.minute ? -1 : 0);
    result.minute = carryoverMinute + lhs.minute - rhs.minute;
    result.hour = carryoverHour + lhs.hour - rhs.hour;
    if (result.hour < 0){
        result.hour += 24;
    }
    return result;
}

int simpletime::roundTime(){
    if (hour == 0 && minute == 0){
        return 0;
    }
    else if (hour < 1){
        return 1;
    }
    else{
        return (minute < 30 ? hour : hour + 1);
    }
}

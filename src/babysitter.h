#include <string>
#include <sstream>
#include <iomanip>
#include "simpletime.h"

#ifndef BABYSITTER_H
#define BABYSITTER_H

class babysitter{

    simpletime start, bed, end;
    simpletime fivePM, elevenFiftyNine, midnight, fourAM;

public:
    babysitter();
    int getCost();
    std::string getStartTime();
    std::string getEndTime();
    std::string getBedtime();

    std::string timeToString(const simpletime &t);

    bool checkValidTimeInput(std::string inputString, simpletime &testTime);
    //parses and puts it into a simpletime struct

    bool setStartTime(std::string inputString);
    bool setBedTime(std::string inputString);
    bool setEndTime(std::string inputString);
};

#endif
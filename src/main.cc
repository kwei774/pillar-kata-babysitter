#include "babysitter.h"
#include <iostream>
using namespace std;


int main(){
    babysitter b;
    string input;
    
    cout << "After entering start, bed, and end times, babysitter will output cost\n";
    cout << "Time format is HH:mmXX\nHH is hours from 01-12\nmm is minutes from 00-59\nXX is either AM or PM\n";
    
    cout << "Enter start time: ";
    cin >> input;
    if (!b.setStartTime(input)){
        cout << "Invalid start time, defaulting to 5:00PM\n";
    }

    cout << "Enter bed time: ";
    cin >> input;
    if (!b.setBedTime(input)){
        cout << "Invalid bed time, defaulting to 12:00AM\n";
    }

    cout << "Enter end time: ";
    cin >> input;
    if (!b.setEndTime(input)){
        cout << "Invalid end time, defaulting to 4:00AM\n";
    }
    if (b.getCost() == -1){
        cout << "Start time is after end time, please try again\n";
    }
    else{
        cout << "Cost: " << b.getCost() << endl;
    }
    return 0;
}

#include "babysitter.h"
#include <iostream>

babysitter::babysitter(){
    start.hour = 17;
    start.minute = 0;
    bed.hour = 0;
    bed.minute = 0;
    end.hour = 4;
    end.minute = 0;

    fivePM.hour = 17;
    fivePM.minute = 0;

    elevenFiftyNine.hour = 23;
    elevenFiftyNine.minute = 59;

    midnight.hour = 0;
    midnight.minute = 0;

    fourAM.hour = 4;
    fourAM.minute = 0;
}


int babysitter::getCost(){
    int beforeBedtimeRate = 12;
    int afterBedtimeBeforeMidnightRate = 8;
    int afterMidnightRate = 16;
    int cost = 0;

    if ( (start >= fivePM && end >= fivePM  && start > end)
        || (start <= fourAM && end <= fourAM && start > end) 
        || (start <= fourAM && end >= fivePM) ){
        return -1;
    }

    if (start >= fivePM && end >= fivePM){
        if (bed <= start && bed >= fivePM){
            cost += (end - start).roundTime() * afterBedtimeBeforeMidnightRate;
        }
        else if (bed > start && bed < end){
            cost += (bed - start).roundTime() * beforeBedtimeRate;
            cost += (end - bed).roundTime() * afterBedtimeBeforeMidnightRate;
        }
        else{
            cost += (end - start).roundTime() * beforeBedtimeRate;
        }
    }
    else if (start <= fourAM && end <= fourAM){
            cost += (end - start).roundTime() * afterMidnightRate;
    }
    else{
        if (bed <= start && bed >= fivePM){
            cost += (midnight - start).roundTime() * afterBedtimeBeforeMidnightRate;
            cost += (end - midnight).roundTime() * afterMidnightRate;
        }
        else if (bed <= fourAM){
            cost += (midnight - start).roundTime() * beforeBedtimeRate;
            cost += (end - midnight).roundTime() * afterMidnightRate;
        }
        else{
            cost += (bed - start).roundTime() * beforeBedtimeRate;
            cost += (midnight - bed).roundTime() * afterBedtimeBeforeMidnightRate;
            cost += (end - midnight).roundTime() * afterMidnightRate;
        }
    }
    return cost;
}

std::string babysitter::timeToString(const simpletime &t){
    std::stringstream ss;
    if (t.hour == 0){
        ss << t.hour + 12;
    }
    else if (t.hour > 12){
        ss << t.hour - 12;
    }
    else{
        ss << t.hour;
    }
    ss << ":";
    ss << std::setw(2) << std::setfill('0') << t.minute;
    if (t.hour > 12){
        ss << "PM";
    }
    else{
        ss << "AM";
    }
    return ss.str();

}

std::string babysitter::getStartTime(){
    return timeToString(start);
}

std::string babysitter::getEndTime(){
    return timeToString(end);
}

std::string babysitter::getBedtime(){
    return timeToString(bed);
}

bool babysitter::checkValidTimeInput(std::string inputString, simpletime &testTime){

    std::stringstream ss;
    char colon;
    std::string testMeridiem;

    bool validTime = false;

    ss << inputString;
    
    ss >> testTime.hour;
    ss >> colon;
    ss >> testTime.minute;
    ss >> testMeridiem;

    if ( inputString.size() > 7
        || ss.fail()
        || (testTime.hour < 1)
        || (testTime.hour > 12)
        || (colon != ':')
        || (testTime.minute < 0)
        || (testTime.minute > 59)
        || (testMeridiem != "AM" && testMeridiem != "PM")
        ){
        validTime = false;
    }

    else{
        if (testMeridiem == "PM"){
            testTime.hour += 12;
        }
        else if (testMeridiem == "AM" && testTime.hour == 12){
            testTime.hour -= 12;
        }
        
        if ( ( testTime >= midnight && testTime <= fourAM)
                || (testTime >= fivePM && testTime <= elevenFiftyNine) ){
            validTime = true;
        }

    }
    return validTime;
}

bool babysitter::setStartTime(std::string inputString){
    simpletime testTime;

    if ( checkValidTimeInput(inputString, testTime) ){
        start = testTime;
        return true;
    }
    else{
        return false;
    }
}

bool babysitter::setBedTime(std::string inputString){
    simpletime testTime;
    
    if ( checkValidTimeInput(inputString, testTime) ){
        bed = testTime;
        return true;
    }
    else{
        return false;
    }
}

bool babysitter::setEndTime(std::string inputString){
    simpletime testTime;

    if ( checkValidTimeInput(inputString, testTime) ){
        end = testTime;
        return true;
    }
    else{
        return false;
    }
}


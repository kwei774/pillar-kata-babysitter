# Babysitter Kata

Kevin's tests and code sample for Pillar Technology's kata

## Kata Background
This kata simulates a babysitter working and getting paid for one night.  The rules are pretty straight forward.

The babysitter:
* starts no earlier than 5:00PM
* leaves no later than 4:00AM
* gets paid $12/hour from start-time to bedtime
* gets paid $8/hour from bedtime to midnight
* gets paid $16/hour from midnight to end of job
* gets paid for full hours (no fractional hours)


## Kata User Story
*As a babysitter
In order to get paid for 1 night of work
I want to calculate my nightly charge*


## Assumptions:
* Babysitter wants to maximize money per night of work and be "fair"
	* Default start time 5:00PM
	* Default bedtime 12:00AM
	* Default end time 4:00AM
	* Maximum money per night: 148
* If input is incorrectly formatted, use default values
* Babysitter will charge at least one hour at whatever rate is appropriate, otherwise babysitter will round to nearest hour
	* 1 - 59 minutes = 1 hour * hourly rate
	* n hour and 0 - 29 minutes = n hours * hourly rate
	* n hour and 30 - 59 minutes = (n + 1) hours * hourly rate
		* Extreme example:
			* 11:58PM start time
			* 11:59PM bed time
			* 12:01 end time
			* This results in 1 hour of start-time to bedtime rate, 1 hour of bedtime to midnight rate, and 1 hour of midnight to end of job rate for a grand total of $36



## Build information

Tested on Ubuntu Linux 16.04 LTS
g++ 5.4.0
cmake 3.5.1
git 2.7.4

Building on ubuntu
```
mkdir build
cd build
cmake ..
make
make test
./main

./simpletime_test
./babysitter_test
```

make test runs cmake tests
for more detailed test information, you can run each test individually (./babysitter_test, ./simpletime_test)

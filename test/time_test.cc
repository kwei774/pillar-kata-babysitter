#include <stdio.h>
#include <string>

#include "gtest/gtest.h"
#include "simpletime.h"

namespace
{
class simpletimeTest : public ::testing::Test {
    protected:
    // You can remove any or all of the following functions if its body
    // is empty.

    simpletimeTest() {
    // You can do set-up work for each test here.
        initTime.hour = 5;
        initTime.minute = 30;

        largerHour.hour = 6;
        largerHour.minute = 20;

        largerMinute.hour = 5;
        largerMinute.minute = 35;
    }

    virtual ~simpletimeTest() {
    // You can do clean-up work that doesn't throw exceptions here.
    }

    // If the constructor and destructor are not enough for setting up
    // and cleaning up each test, you can define the following methods:

    virtual void SetUp() {
    // Code here will be called immediately after the constructor (right
    // before each test).
    }

    virtual void TearDown() {
    // Code here will be called immediately after each test (right
    // before the destructor).
    }

    // Objects declared here can be used by all tests in the test case for Foo.
    simpletime initTime, largerHour, largerMinute;

};

TEST_F(simpletimeTest, checkLessThanOperatorHourTrue){
    EXPECT_EQ(true, (initTime < largerHour));
}

TEST_F(simpletimeTest, checkLessThanOperatorMinuteTrue){
    EXPECT_EQ(true, (initTime < largerMinute));
}

TEST_F(simpletimeTest, checkLessThanOperatorHourFalses){
    EXPECT_EQ(false, (largerHour < initTime));
}

TEST_F(simpletimeTest, checkLessThanOperatorMinuteFalses){
    EXPECT_EQ(false, (largerMinute < initTime));
}


TEST_F(simpletimeTest, checkLessThanOrEqualOperatorMinuteTrueLess){
    EXPECT_EQ(true, (initTime <= largerMinute));
}

TEST_F(simpletimeTest, checkLessThanOrEqualOperatorMinuteTrueEqual){
    EXPECT_EQ(true, (initTime <= initTime));
}

TEST_F(simpletimeTest, checkLessThanOrEqualOperatorMinuteFalse){
    EXPECT_EQ(false, (largerMinute <= initTime));
}

TEST_F(simpletimeTest, checkGreaterThanOperatorTrue){
    EXPECT_EQ(true, (largerMinute > initTime));
}

TEST_F(simpletimeTest, checkGreaterThanOperatorFalse){
    EXPECT_EQ(false, (initTime > largerMinute));
}

TEST_F(simpletimeTest, checkGreaterThanOrEqualOperatorTrueGreater){
    EXPECT_EQ(true, (largerMinute >= initTime));
}

TEST_F(simpletimeTest, checkGreaterThanOrEqualOperatorTrueEqual){
    EXPECT_EQ(true, (initTime >= initTime));
}

TEST_F(simpletimeTest, checkGreaterThanOrEqualOperatorFalse){
    EXPECT_EQ(false, (initTime >= largerMinute));
}

TEST_F(simpletimeTest, checkEqualityOperatorTrue){
    EXPECT_EQ(true, (initTime == initTime));
}

TEST_F(simpletimeTest, checkEqualityOperatorFalse){
    EXPECT_EQ(false, (initTime == largerMinute));
}

TEST_F(simpletimeTest, checkInequalityOperatorTrue){
    EXPECT_EQ(true, (initTime != largerMinute));
}

TEST_F(simpletimeTest, checkInequalityOperatorFalse){
    EXPECT_EQ(false, (initTime != initTime));
}

TEST_F(simpletimeTest, checkAssignmentOperator){
    simpletime testTime = initTime;
    EXPECT_EQ(true, (initTime == testTime));
}

TEST_F(simpletimeTest, checkSubtractionOneHour){
    simpletime testTime, testTimeSub1;
    testTime.hour = 6;
    testTime.minute = 0;
    testTimeSub1.hour = 5;
    testTimeSub1.minute = 0;
    simpletime final = testTime - testTimeSub1;
    EXPECT_EQ(1, final.hour);
}

TEST_F(simpletimeTest, checkSubtractionOneMinute){
    simpletime testTime, testTimeSub1;
    testTime.hour = 6;
    testTime.minute = 1;
    testTimeSub1.hour = 6;
    testTimeSub1.minute = 0;
    simpletime final = testTime - testTimeSub1;
    EXPECT_EQ(0, final.hour);
    EXPECT_EQ(1, final.minute);
}

TEST_F(simpletimeTest, checkSubtractionCarryOverMinute){
    simpletime testTime, testTimeSub1;
    testTime.hour = 6;
    testTime.minute = 30;
    testTimeSub1.hour = 5;
    testTimeSub1.minute = 45;
    simpletime final = testTime - testTimeSub1;
    EXPECT_EQ(0, final.hour);
    EXPECT_EQ(45, final.minute);
}

TEST_F(simpletimeTest, checkSubtractionCarryMidnight){
    simpletime testTime, testTimeSub1;
    testTime.hour = 6;
    testTime.minute = 30;
    testTimeSub1.hour = 17;
    testTimeSub1.minute = 45;
    simpletime final = testTime - testTimeSub1;
    EXPECT_EQ(12, final.hour);
    EXPECT_EQ(45, final.minute);
}

TEST_F(simpletimeTest, checkSubtractionZeroResult){
    
    simpletime final = initTime - initTime;
    EXPECT_EQ(0, final.hour);
    EXPECT_EQ(0, final.minute);
}

TEST_F(simpletimeTest, checkSubtractionFromZero){
    simpletime testTime, testTimeSub1;
    testTime.hour = 0;
    testTime.minute = 0;
    testTimeSub1.hour = 17;
    testTimeSub1.minute = 45;
    simpletime final = testTime - testTimeSub1;
    EXPECT_EQ(6, final.hour);
    EXPECT_EQ(15, final.minute);
}

TEST_F(simpletimeTest, checkRoundTimeLessThanOne){
    simpletime testTime;
    testTime.hour = 0;
    testTime.minute = 1;
    EXPECT_EQ(1, testTime.roundTime());
}

TEST_F(simpletimeTest, checkRoundTimeDown){
    simpletime testTime;
    testTime.hour = 1;
    testTime.minute = 1;
    EXPECT_EQ(1, testTime.roundTime());
}

TEST_F(simpletimeTest, checkRoundTimeUp){
    simpletime testTime;
    testTime.hour = 1;
    testTime.minute = 30;
    EXPECT_EQ(2, testTime.roundTime());
}

TEST_F(simpletimeTest, checkRoundZero){
    simpletime testTime;
    testTime.hour = 0;
    testTime.minute = 0;
    EXPECT_EQ(0, testTime.roundTime());
}


}

GTEST_API_ int main(int argc, char **argv) {
  printf("Running main() from time_test.cc\n");
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}

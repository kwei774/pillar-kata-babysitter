#include <stdio.h>
#include <string>

#include "gtest/gtest.h"
#include "babysitter.h"

namespace
{


// The fixture for testing class Foo.
class babysitterTest : public ::testing::Test {
 protected:
  // You can remove any or all of the following functions if its body
  // is empty.

  babysitterTest() {
    // You can do set-up work for each test here.
  }

  virtual ~babysitterTest() {
    // You can do clean-up work that doesn't throw exceptions here.
  }

  // If the constructor and destructor are not enough for setting up
  // and cleaning up each test, you can define the following methods:

  virtual void SetUp() {
    // Code here will be called immediately after the constructor (right
    // before each test).
  }

  virtual void TearDown() {
    // Code here will be called immediately after each test (right
    // before the destructor).
  }

  // Objects declared here can be used by all tests in the test case for Foo.
  babysitter b;
};

TEST_F(babysitterTest, checkDefaultConstructorCost){
    EXPECT_EQ(148, b.getCost());
}

TEST_F(babysitterTest, checkDefaultConstructorStartTime){
    EXPECT_EQ("5:00PM", b.getStartTime());
}

TEST_F(babysitterTest, checkDefaultConstructorEndTime){
    EXPECT_EQ("4:00AM", b.getEndTime());
}

TEST_F(babysitterTest, checkDefaultConstructorBedtime){
    EXPECT_EQ("12:00AM", b.getBedtime());
}

TEST_F(babysitterTest, setStartValidTime){
    EXPECT_EQ(true, b.setStartTime("5:00PM"));
}

TEST_F(babysitterTest, changeStartHourValid){
    EXPECT_EQ(true, b.setStartTime("6:00PM"));
    EXPECT_EQ("6:00PM", b.getStartTime());
}

TEST_F(babysitterTest, changeStartMinuteValid){
    EXPECT_EQ(true, b.setStartTime("5:01PM"));
    EXPECT_EQ("5:01PM", b.getStartTime());
}

TEST_F(babysitterTest, changeStartPMtoAMValid){
    EXPECT_EQ(true, b.setStartTime("3:00AM"));
    EXPECT_EQ("3:00AM", b.getStartTime());
}

TEST_F(babysitterTest, changeStartHourBeforeValidTime){
    EXPECT_EQ(false, b.setStartTime("4:00PM"));
    EXPECT_EQ("5:00PM", b.getStartTime());
}

TEST_F(babysitterTest, changeStartHourAfterValidTime){
    EXPECT_EQ(false, b.setStartTime("5:00AM"));
    EXPECT_EQ("5:00PM", b.getStartTime());
}

TEST_F(babysitterTest, changeStartHourtoMidnight){
    EXPECT_EQ(true, b.setStartTime("12:00AM"));
    EXPECT_EQ("12:00AM", b.getStartTime());
}

TEST_F(babysitterTest, changeStartTimeToBeforeValidTime){
    EXPECT_EQ(false, b.setStartTime("4:45PM"));
    EXPECT_EQ("5:00PM", b.getStartTime());
}

TEST_F(babysitterTest, changeStartTimeToAfterValidTime){
    EXPECT_EQ(false, b.setStartTime("4:45AM"));
    EXPECT_EQ("5:00PM", b.getStartTime());
}

TEST_F(babysitterTest, changeStartTimeToAfterLargeInvalidAMHour){
    EXPECT_EQ(false, b.setStartTime("123:45AM"));
    EXPECT_EQ("5:00PM", b.getStartTime());
}

TEST_F(babysitterTest, setEndTimeHourValid){
    EXPECT_EQ(true, b.setEndTime("6:00PM"));
    EXPECT_EQ("6:00PM", b.getEndTime());
}

TEST_F(babysitterTest, calculateCostOneHourPM){
    EXPECT_EQ(true, b.setStartTime("6:00PM"));
    EXPECT_EQ(true, b.setEndTime("7:00PM"));
    EXPECT_EQ(12, b.getCost());
}

TEST_F(babysitterTest, calculateCostOneHourAM){
    EXPECT_EQ(true, b.setStartTime("1:00AM"));
    EXPECT_EQ(true, b.setEndTime("2:00AM"));
    EXPECT_EQ(16, b.getCost());
}

TEST_F(babysitterTest, setBedTimeHourValid){
    EXPECT_EQ(true, b.setBedTime("6:00PM"));
    EXPECT_EQ("6:00PM", b.getBedtime());
}

TEST_F(babysitterTest, setStartTimeInvalidCharHourInput){
    EXPECT_EQ(false, b.setStartTime("A1:12AM"));
    EXPECT_EQ("5:00PM", b.getStartTime());
}

TEST_F(babysitterTest, setStartTimeInvalidCharBeforeMinute){
    EXPECT_EQ(false, b.setStartTime("1:X1AM"));
    EXPECT_EQ("5:00PM", b.getStartTime());
}

TEST_F(babysitterTest, setStartTimeInvalidCharAfterMinute){
    EXPECT_EQ(false, b.setStartTime("1:1XAM"));
    EXPECT_EQ("5:00PM", b.getStartTime());
}

TEST_F(babysitterTest, setStartTimeInvalidNoColon){
    EXPECT_EQ(false, b.setStartTime("101AM"));
    EXPECT_EQ("5:00PM", b.getStartTime());
}

TEST_F(babysitterTest, setStartTimeInvalidColonChar){
    EXPECT_EQ(false, b.setStartTime("12-1AM"));
    EXPECT_EQ("5:00PM", b.getStartTime());
}

TEST_F(babysitterTest, setStartTimeInvalidCharBeforeHour){
    EXPECT_EQ(false, b.setStartTime("A1:01AM"));
    EXPECT_EQ("5:00PM", b.getStartTime());
}

TEST_F(babysitterTest, setStartTimeInvalidCharAfterHour){
    EXPECT_EQ(false, b.setStartTime("1B:01AM"));
    EXPECT_EQ("5:00PM", b.getStartTime());
}

TEST_F(babysitterTest, setStartTimeInvalidNegativeHour){
    EXPECT_EQ(false, b.setStartTime("-1:01AM"));
    EXPECT_EQ("5:00PM", b.getStartTime());
}

TEST_F(babysitterTest, setStartTimeInvalidZeroHourAM){
    EXPECT_EQ(false, b.setStartTime("0:01AM"));
    EXPECT_EQ("5:00PM", b.getStartTime());
}

TEST_F(babysitterTest, setStartTimeInvalidNegativeHourPM){
    EXPECT_EQ(false, b.setStartTime("-11:01PM"));
    EXPECT_EQ("5:00PM", b.getStartTime());
}

TEST_F(babysitterTest, setStartTimeInvalidNegativeMinute){
    EXPECT_EQ(false, b.setStartTime("1:-1AM"));
    EXPECT_EQ("5:00PM", b.getStartTime());
}

TEST_F(babysitterTest, setStartTimeInvalidLargeMinute){
    EXPECT_EQ(false, b.setStartTime("1:100AM"));
    EXPECT_EQ("5:00PM", b.getStartTime());
}

TEST_F(babysitterTest, setStartTimeLargeWhitespaceBeforeHour){
    EXPECT_EQ(false, b.setStartTime("                        5:00PM"));
    EXPECT_EQ("5:00PM", b.getStartTime());
}

TEST_F(babysitterTest, setStartTimeInvalidHourAM){
    EXPECT_EQ(false, b.setStartTime("18:00AM"));
    EXPECT_EQ("5:00PM", b.getStartTime());
}

//at most 8 permutations of rates

TEST_F(babysitterTest, costofTZeroHours){
    EXPECT_EQ(true, b.setStartTime("6:00PM"));
    EXPECT_EQ(true, b.setEndTime("6:00PM"));
    EXPECT_EQ(0, b.getCost());
}

TEST_F(babysitterTest, costofOnlyBedTimetoMidnightRate){
    EXPECT_EQ(true, b.setStartTime("6:00PM"));
    EXPECT_EQ(true, b.setBedTime("6:00PM"));
    EXPECT_EQ(true, b.setEndTime("6:01PM"));
    EXPECT_EQ(8, b.getCost());
}

TEST_F(babysitterTest, costofOnlyStartTimetoBedtimeRate){
    EXPECT_EQ(true, b.setStartTime("6:00PM"));
    EXPECT_EQ(true, b.setEndTime("6:01PM"));
    EXPECT_EQ(12, b.getCost());
}

TEST_F(babysitterTest, costofAfterMidnightRate){
    EXPECT_EQ(true, b.setStartTime("2:00AM"));
    EXPECT_EQ(true, b.setEndTime("3:01AM"));
    EXPECT_EQ(16, b.getCost());
}

TEST_F(babysitterTest, costofStartBedBeforeMidnightRates){
    EXPECT_EQ(true, b.setStartTime("6:05PM")); //$24
    EXPECT_EQ(true, b.setBedTime("8:15PM")); //$8
    EXPECT_EQ(true, b.setEndTime("9:30PM"));
    EXPECT_EQ(32, b.getCost());
}

TEST_F(babysitterTest, costofStartTimeAndAftereMidnightRates){
    EXPECT_EQ(true, b.setStartTime("6:05PM")); //$72
    EXPECT_EQ(true, b.setEndTime("1:29AM")); //16
    EXPECT_EQ(88, b.getCost());
}

TEST_F(babysitterTest, costofBedtimeAndAfterMidnightRates){
    EXPECT_EQ(true, b.setStartTime("7:05PM"));
    EXPECT_EQ(true, b.setBedTime("6:00PM"));
    EXPECT_EQ(true, b.setEndTime("2:30AM"));
    EXPECT_EQ(88, b.getCost());
}

TEST_F(babysitterTest, costofAllThreeRates){
    EXPECT_EQ(true, b.setStartTime("6:05PM")); //$24
    EXPECT_EQ(true, b.setBedTime("8:00PM")); //$32
    EXPECT_EQ(true, b.setEndTime("2:30AM")); //$48
    EXPECT_EQ(104, b.getCost());
}

TEST_F(babysitterTest, costofOnlyStartTimetoBedtimeRateWithAMBedtime){
    EXPECT_EQ(true, b.setStartTime("6:00PM"));
    EXPECT_EQ(true, b.setBedTime("1:00AM"));
    EXPECT_EQ(true, b.setEndTime("6:01PM"));
    EXPECT_EQ(12, b.getCost());
}

TEST_F(babysitterTest, costofStartTimeAndAftereMidnightRateswithAMBedtimeGreaterThanEndTime){
    EXPECT_EQ(true, b.setStartTime("6:05PM")); //$72
    EXPECT_EQ(true, b.setEndTime("1:29AM")); //16
    EXPECT_EQ(true, b.setBedTime("3:29AM"));
    EXPECT_EQ(88, b.getCost());
}

TEST_F(babysitterTest, costofStartTimeAndAftereMidnightRateswithAMBedtimeLessThanEndTime){
    EXPECT_EQ(true, b.setStartTime("6:05PM")); //$72
    EXPECT_EQ(true, b.setEndTime("1:29AM")); //16
    EXPECT_EQ(true, b.setBedTime("12:29AM"));
    EXPECT_EQ(88, b.getCost());
}

TEST_F(babysitterTest, invalidCostStartAfterEndPM){
    EXPECT_EQ(true, b.setStartTime("8:00PM"));
    EXPECT_EQ(true, b.setEndTime("6:01PM"));
    EXPECT_EQ(-1, b.getCost());
}

TEST_F(babysitterTest, invalidCostStartAfterEndAM){
    EXPECT_EQ(true, b.setStartTime("3:00AM"));
    EXPECT_EQ(true, b.setEndTime("1:01AM"));
    EXPECT_EQ(-1, b.getCost());
}

TEST_F(babysitterTest, invalidCostStartAfterEndMixed){
    EXPECT_EQ(true, b.setStartTime("3:00AM"));
    EXPECT_EQ(true, b.setEndTime("9:01PM"));
    EXPECT_EQ(-1, b.getCost());
}



}


GTEST_API_ int main(int argc, char **argv) {
  printf("Running main() from main.cc\n");
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}

